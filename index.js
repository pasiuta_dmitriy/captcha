
const puppeteer = require('puppeteer');
const request = require('request-promise-native');
const poll = require('promise-poller').default;

const config = {
    sitekey: '6LeTnxkTAAAAAN9QEuDZRpn90WwKk_R1TRW_g-JC',
    pageurl: 'https://old.reddit.com/login',
    apikey:'4263bff7759c3097e238669d0e68d9d6',
    apiSubmitUrl:'http://2captcha.com/in.php',
    apiRetrieveUrl: 'http://2captcha.com/res.php'

}
const getUsername=function (){
    return 'dimadima2000diablo'
};
const getPassword=function (){
    return 'p@ssword1234DimaHello';
};


const chromeOptions = {
    executablePath:'/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    headless:false,
    slowMo:10,
    defaultViewport: null
};

(async function main() {
    const browser = await puppeteer.launch(chromeOptions);

    const page = await browser.newPage();

    console.log(`Navigation to ${config.pageurl}`);
    await page.goto(config.pageurl);

    const requestId = await initiateCaptchaRequest(config.apikey);

    const username= getUsername();
    console.log(`Typing username ${username}`);
    await page.type('#user_reg', username);

    const password= getPassword();
    console.log(`Typing password ${password}`);
    await page.type('#passwd_reg', password);
    await page.type('#passwd2_reg', password);

    const response= await pollForRequestResults(config.apikey, requestId);
    console.log(`Entering recaptcha response ${response}`);
    await page.evaluate(`document.getElementById("g-recaptcha-response").innerHTML="${response}";`);



console.log(`Submitting...`);
page.click('#register-form button[type=submit]');
})()

async function initiateCaptchaRequest(apiKey) {
    console.log(apiKey);
    const formData = {
        method: 'userrecaptcha',
        googlekey: config.sitekey,
        key: apiKey,
        pageurl: config.pageurl,
        json: 1
    };
    console.log(`Submitting solution request to 2captcha for ${config.pageurl}`);
    const response = await request.post(config.apiSubmitUrl, {form: formData});
    return JSON.parse(response).request;
}

async function pollForRequestResults(key, id, retries = 30, interval = 1500, delay = 15000) {
    console.log(`Waiting for ${delay} milliseconds`)
    await timeout(delay);
    return poll({
        taskFn: requestCaptchaResults(key, id),
        interval,
        retries
    });
}

function requestCaptchaResults(apiKey, requestId) {
    const url = `${config.apiRetrieveUrl}?key=${apiKey}&action=gey&Id=${requestId}&json=1`;
    return async function() {
        return new Promise(async function(resolve, reject){
            console.log(`Polling for responce...`)
            const rawResponse = await request.get(url);
            const resp = JSON.parse(rawResponse);
            console.log(resp)
            if (resp.status === 0) return reject(resp.request);
            console.log('Response received.')
            resolve(resp.request);
        });
    }
}

const timeout = millis => new Promise(resolve => setTimeout(resolve, millis))